import Request from '@/utils/luch-request/index.js' // 下载的插件
const baseURL = 'http://124.93.196.45:10001'
const http = new Request();
// 不加token拦截器的请求使用
const httpNoAuth = new Request();

http.setConfig((config) => {
	config.baseURL = baseURL
	return config;
})

httpNoAuth.setConfig((config) => {
	config.baseURL = baseURL
	return config;
})

//请求之前拦截
http.interceptors.request.use((config) => {
	config.header = {
		'Authorization': 'Bearer ' + uni.getStorageSync('token')
	}
	return config;
})

//请求之后拦截
http.interceptors.response.use((response) => {
	if (response.data.code == 401) {
		//登陆失效，直接跳转到登陆界面
		uni.navigateTo({
			url:'/pages/login/login.vue'
		});
		return "updateToken";
	}
	return response;
})





export {
	http,
	httpNoAuth,
	baseURL
}
