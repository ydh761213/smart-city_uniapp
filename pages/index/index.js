import {baseURL}  from '@/api/httputil'


const newListDataHandled=(data)=>{
	let result=[];
	data.forEach(item=>{
		let resultItem={};
		resultItem.cover= baseURL+item.cover;
		resultItem.title=item.title;
		resultItem.body=item.content;
		resultItem.name=item.createBy;
		resultItem.time=item.publishDate;
		resultItem.id=item.id
		result.push(resultItem)
	});
	//一定要记得return
	return result;
}

/*处理返回的服务数据*/
const serviceDataHandled=(data) =>{
	let result=[];
	data.data.forEach(data1 =>{
		let resultItem={};
		resultItem.name=data1.liveName;
		resultItem.imgUrl=baseURL+data1.imgUrl;
		result.push(resultItem);
	});
	console.log(result);
	return result;
}


export {
	newListDataHandled,serviceDataHandled
}